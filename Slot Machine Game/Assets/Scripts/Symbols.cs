﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Symbols", menuName = "SlotSymbols")]
public class Symbols : ScriptableObject {
    public int value;
    public Sprite symbolSprite;

}