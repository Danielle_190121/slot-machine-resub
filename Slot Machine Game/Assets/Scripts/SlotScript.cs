﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SlotScript : MonoBehaviour {

  [SerializeField] List<Symbols> Possiblesymbools = new List<Symbols> ();
  [SerializeField] List<Symbols> ReelOne = new List<Symbols> ();
  [SerializeField] List<Symbols> Reeltwo = new List<Symbols> ();
  [SerializeField] List<Symbols> Reelthree = new List<Symbols> ();

  public Image ImageOneOne;
  public Image ImageOneTwo;
  public Image ImageOneThree;

  public Image ImageTwoOne;
  public Image ImageTwoTwo;
  public Image ImageTwoThree;

  public Image ImageThreeOne;
  public Image ImageThreeTwo;
  public Image ImageThreeThree;
  public Text coinText;
  public int coins = 100;

  public GameObject winUI;
  public GameObject winUIV;
  public GameObject winUID;
  public Text txt;
  public int betAmount = 10;

  public Text bet;

  private bool one;
  private bool two;
  private bool three;

  private int rowCount;

  AudioSource sound;

  private void Start () {
    coinText.text = coins.ToString ();
    sound = GetComponent<AudioSource> ();
    bet.text = betAmount.ToString ();
    winUI.SetActive (false);
    winUID.SetActive (false);
    winUIV.SetActive (false);
  }

  private void FixedUpdate () {
    coinText.text = coins.ToString ();
    bet.text = betAmount.ToString ();

  }

  public void Spin () {

    rowCount = 0;

    Bet ();

    one = false;
    two = false;
    three = false;

    winUI.SetActive (false);
    winUID.SetActive (false);
    winUIV.SetActive (false);

    for (int i = 0; i < 3; i++) {
      ReelOne[i] = Possiblesymbools[Random.Range (0, Possiblesymbools.Count)];
      Reeltwo[i] = Possiblesymbools[Random.Range (0, Possiblesymbools.Count)];
      Reelthree[i] = Possiblesymbools[Random.Range (0, Possiblesymbools.Count)];
    }

    ImageOneOne.sprite = ReelOne[0].symbolSprite;
    ImageOneTwo.sprite = ReelOne[1].symbolSprite;
    ImageOneThree.sprite = ReelOne[2].symbolSprite;

    ImageTwoOne.sprite = Reeltwo[0].symbolSprite;
    ImageTwoTwo.sprite = Reeltwo[1].symbolSprite;
    ImageTwoThree.sprite = Reeltwo[2].symbolSprite;

    ImageThreeOne.sprite = Reelthree[0].symbolSprite;
    ImageThreeTwo.sprite = Reelthree[1].symbolSprite;
    ImageThreeThree.sprite = Reelthree[2].symbolSprite;

    //check horizontal

    for (int i = 0; i < 3; i++) {
      if (ReelOne[i] == Reeltwo[i] && ReelOne[i] == Reelthree[i]) {
        winUI.SetActive (true);
        PlaySound ();
        NormWIn ();
      }
    }

    //check diagonal
    if (ReelOne[0] == Reeltwo[1] && Reeltwo[1] == Reelthree[2]) {
      DiagonalWin ();
      winUID.SetActive (true);

    }

    if (ReelOne[2] == Reeltwo[1] && Reeltwo[1] == Reelthree[0]) {
      winUID.SetActive (true);
      DiagonalWin ();
    }

    //check vertical

    if (ReelOne[0] == ReelOne[1] && ReelOne[1] == ReelOne[2]) {
      winUIV.SetActive (true);
      VertWin ();
      rowCount++;
    }

    if (Reeltwo[0] == Reeltwo[1] && Reeltwo[1] == Reeltwo[2]) {
      winUIV.SetActive (true);
      VertWin ();
      rowCount++;
    }

    if (Reelthree[0] == Reelthree[1] && Reelthree[1] == Reelthree[2]) {
      winUIV.SetActive (true);
      VertWin ();
      rowCount++;
    }

    if (one && two || two && three || one && three) {
      Debug.Log ("Win twice");
      Twice ();
    }

    if (one && two && three) {
      Debug.Log ("Win thrice");
      Thrice ();
    }

    if (rowCount >= 2) {
      Debug.Log ("Two rows");
      Thrice ();
    }
  }
  public void NormWIn () {
    coins += 20;
    one = true;
  }

  public void DiagonalWin () {
    three = true;
    PlaySound ();
    coins += (betAmount * 2);
  }

  public void VertWin () {
    two = true;
    PlaySound ();
    coins += 30;
  }

  public void Twice () {
    coins += 50;
  }

  public void Thrice () {
    coins += 70;
  }

  public void PlaySound () {
    sound.Play ();
  }

  public void Bet () {
    coins -= betAmount;
  }

  public void IncreaseBet () {
    betAmount += 10;
  }
  public void DecreaseBet () {
    betAmount -= 10;
  }
}