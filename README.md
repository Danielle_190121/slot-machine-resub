This is a framework for a casino slotmachine.

The game is intended for a 3x3 grid, thus three lines and three rows. 

There is a possible outcome of four symbols, though more can be added.

Whenever you pull, the slots are randomized, giving you any one of the four symbols.

Whenever you spin, it checks for horizontal, vertical or diagonal matches.

Landing more matches per vertical line and landing combos with the matches earns bigger bonuses.